import Lexer from 'lex'
import Jison from 'jison'
// Jison.
var Parser = Jison.Parser;

var relational = {
  precedence: 3,
  associativity: "left"
};

var equality = {
  precedence: 2,
  associativity: "left"
};
// var Lexer = require("lex");
 
// var grammar = {
  
//     "bnf": {
//       // "hex_strings" :[ "hex_strings HEX", "HEX" ]
//         "expression" :[[ "e EOF",   "return $1;"  ]],
//         "e" :[[ "NUMBER",  "$$ = Number(yytext);" ]],
//         "assign" :[[ "EXPRESSION ASSIGN NUMBER EOF",  "$$ = $3;" ]]
//     }
// };
 
var grammar = {
    "lex": {
        "rules": [
           ["\\s+", "/* skip whitespace */"],
           ["[0-9]+", "return 'NUMBER';"],
           ["val","return 'VARIABLE';"],
           ["[a-z]+","return 'STRING'"],
           ["/[0-9]+\.[0-9]+/", "return FLOAT"],
           ["/\+/i", "return SUM"],
           ["/\-/i", "return SUBSTRACT"],
           ["/\*/i", "return MULTIPLY"],
           ["\//i", "return DIVIDE"],
           ["/\=/i", "return ASSIGN"],
           ["/=/", "return EQUALS"]          
        ]
    },

    "operators": {
        "<":  relational,
        "<=": relational,
        ">":  relational,
        ">=": relational,
        "==": equality,
        "!=": equality,
        "&&": {
            precedence: 1,
            associativity: "right"
        },
        "||": {
            precedence: 0,
            associativity: "right"
        }
    },

    "bnf": {
        "e" :[
            ["VARIABLE string", "$$ = {name:{contains:$2}};"],
            ["STRING", "$$ = $1;"],
            ["NUMBER + NUMBER", "$1+$2"],
            ["NUMBER - NUMBER", "$1-$2"],
            ["NUMBER * NUMBER", "$1*$2"],
            ["NUMBER / NUMBER", "$1/$2"],
        ],
        "string":[
            ["STRING","$$ = yytext"]
        ]
    }
};
var parser = new Parser(grammar);
var lexer = new Lexer();
 
// lexer.addRule(/\s+/, function () {});

lexer.addRule(/[0-9]+(?:\.[0-9]+)?\b/, function (lexeme) {
    this.yytext = lexeme;
    return "NUMBER";
});
lexer.addRule(/[0-9]+\.[0-9]+/, function (lexeme) {
  return "FLOAT"
});
// lexer.addRule(/[a-f\d]+/i, function (lexeme) {
//   return "HEX";
// });
// lexer.addRule(/[a-zA-Z0-9]+/i, function (lexeme) {
//   return "EXPRESSION";
// });
// lexer.addRule(/\+/i, function (lexeme) {
//   return "SUM";
// });
// lexer.addRule(/\+/, function (lexeme) {
//   return "PLUS";
// });
// lexer.addRule(/\-/, function (lexeme) {
//   return "MINUS"
// });
// lexer.addRule(/\*/, function (lexeme) {
//   return "ASTERISK"
// });
// lexer.addRule(/\//, function (lexeme) {
//   return "SLASH";
// });
// lexer.addRule(/=/, function (lexeme) {
//   return "EQUALS"
// });
// lexer.addRule(/\=/i, function (lexeme) {
//   return "ASSIGN";
// });
// lexer.addRule(/\n/, function () {
//     return "EOF";
// });
 
// parser.parse("2");
 
// lexer.lex();
const compiler = {
  compileText: (text) => {
    // col = 1
    // row = 1
    // const lines = text.split("\n")
    // for (const line of lines) {
      // if (line != "") {
        parser.parse(text)
      // }
      
      // const exec = parser.generate(line)
    // }
    // const result =parser.parse(text)
    // const result = lexer.scan()
    // lexer.input = text
    // const result = lexer.lex()
    // console.log(lexer.state)
    // return result    
  }
}
export default compiler

const createNode = () => {
  return {
    leftElement: null,
    rightElement: null,
    value: null,
    type: null,
    navigateLCR: function () {
      console.log(this)
      if (this.leftElement) {
        this.leftElement.navigateLCR()
      }
      this.do()
      if (this.rightElement) {
        this.rightElement.navigateLCR()
      }
    },
    do: function () {
      console.log(this.value)
      console.log(this.type)
    }
  }
}

export default {
  createNode
}
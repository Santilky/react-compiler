const tokens = [
  {
    type: 'MEMORYRESERVE',
    regex: (text) => text === 'val'
  },
  {
    type: 'ASSIGNMENT',
    regex: (text) => text === '='
  },
  {
    type: 'COMPARE-EQUAL',
    regex: (text) => text === '=='
  },
  {
    type: 'COMPARE-NOT-EQUAL',
    regex: (text) => text === '!='
  },
  {
    type: 'PRIM-NUMBER-INT',
    regex: (text) => text.match(RegExp(/[0-9]+/))
  },
  {
    type: 'PRIM-STRING',
    regex: (text) => text.match(RegExp(/"(.*?)"/))
  },
  {
    type: 'EXPRETION',
    regex: (text) => text.match(RegExp(/[a-zA-Z0-9]+/))
  },
  {
    type: 'SUM',
    regex: (text) => text.match(RegExp(/\+/))
  }
]
export default tokens
import tokens from './definitions/tokens'



const processText = (text) => {
  
  const texts = text.split(/\s+/)
  for (const value of texts) {
    console.log(value)
    const result = tokens.find(element => element.regex(value))
    if (result) {
      console.log(`Type: ${result.type}, Value: ${value}`)
    } else {
      throw new Error(`${value} is not a valid expression`)
    }
    // const newNode = node.createNode()
    // newNode.navigate()
    
  }
  console.log(texts.length)
}

const textProcessor = {
  processText
}

export default textProcessor
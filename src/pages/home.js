import React, { useState } from 'react'
import lexer from '../compiler/definitions'
import {Button } from 'shards-react'
// import lexAnalizer from '../compiler/textprocessor'
import "bootstrap/dist/css/bootstrap.min.css";
import "shards-ui/dist/css/shards.min.css"

const Home = () => {
  const [code, setCode] = useState("")
  // const textbox = useRef()
  const [lines, setLines] = useState([1])
  const [errors, setErrors] = useState([])
  const compile = () => {
    console.log(code)
    try {
      // lexAnalizer.processText(code)
      const lines = code.split('\n')
      let i = 1;
      const errores = []
      console.log(lines)
      for (const line of lines) {
        
        try {
          if (line != "" || i < lines.length) {
            if (line != "") {
              lexer.compileText(line)
            }
            
          }
          
          
          console.log(`OK: ${line}`)
        } catch (error) {
          console.log()
          errores.push({error, id: i})
        }
        i++
      }
      setErrors(errores)
      // lexer.compileText(code)
    } catch (error) {
      console.log(error.message)
    }
    
  }
  // const convertCodeToInnerHTML = () => {
  //   return {__html: code}
  // }
  const handleTextChange = (event) => {
    // console.log(event.target.innerText)
    setCode(event.target.innerText)
  }
  const setLineNumbers = (textCode) => {
    const lines = textCode.split("\n") 
    
    // if (             Q a)
    let i = 1
    // let text = ""
    const arrayLine = []
    for (const line of lines) {
      if (line != "" || i < lines.length) {
        arrayLine.push(i)
        i++
      } 
      
    }
    
    setLines(arrayLine)
  }
  const getLines = () => {

    return lines.map(line => errors.find(error => error.id == line) === null ? <div><tr>{line + ""}</tr></div> : <div><span className="text-danger"><tr>{line}</tr></span></div>)
  }
  const getErrors = () => {
    // console.log(errors)
    const lines = code.split('\n')
    for (const error of errors) {
      console.log(error)
    }
    return errors.map(error => <li>Error: {lines[error.id - 1]}, { "" + error.error}</li>)
  }
  return <div className="container">
    <div>
      <h3>Compilador</h3>

      <div className="row border">
        <div className="col-sm-1 pl-4 pr-3 border">
          {getLines()}
       </div>
        <div className="col-sm-11 border" contentEditable onInput={text => {
      //  console.log(text)
         handleTextChange(text)
         setLineNumbers(text.target.innerText)}} onIn></div>
      </div>  
    </div>
    
    <div>
      <p>Revisa si la semantica del lenguaje es correcta</p>
    </div>
     
    <Button onClick={() => compile()}>Revisar </Button>
    <ul>
      {getErrors()}
    </ul>
  </div>
}

export default Home